package analysis.smell.cloneutils;

import AST.*;

public class RelevantASTNode {
	public static String[] relevantASTNodeNames = new String[]{
			StringLiteral.class.getName(),
			NumLiteral.class.getName(),
			NullLiteral.class.getName(),
			BooleanLiteral.class.getName(),
			VariableExpBlock.class.getName(),
			DoIfBlock.class.getName(),
			DoRepeatBlock.class.getName(),
			DoIfElseBlock.class.getName(),
			DoForeverBlock.class.getName(),
			DoUntilBlock.class.getName(),
			ParamExpBlock.class.getName(),
			UndefinedBlock.class.getName()
	};
}
