package analysis;

import java.io.StringReader;

import junit.framework.TestCase;
import main.ScratchParser;
import main.Util;
import AST.Program;

public class VariableParsingTest extends TestCase {
	private final static String FILENAME = "/parsing/test-var.json";
	Program program = null;
	public void setup() {
		
	}

	public void test() {
		StringReader srd = null;
		ScratchParser parser = new ScratchParser();
	
		try {
			srd = Util.getStringReaderFromFile(FILENAME);
			program = parser.parse(srd);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		assertEquals(1, program.getStage().getNumVarDecl());
		assertEquals(1, program.getStage().getNumSprite());
		assertEquals(1, program.getStage().getSprite(0).getNumVarDecl());
		
	}
}