package analysis.smell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import main.Util;
import AST.BlockExp;
import AST.Scriptable;
import analysis.Analysis;

public class InappropriateIntimacyAnalysis extends Analysis {

	public static final String SMELL_NAME = "Inappropriate Intimacy";
	public static int threshold = 4; // 70th percentile
	private Map<Scriptable, List<BlockExp>> resultMap;
	private Map<Scriptable, Interaction> interactions;

	@Override
	public void initialize() {
		// TODO Auto-generated method stub

	}

	class Interaction {
		@Override
		public String toString() {
			return "Interaction [actor=" + actor + ", interactionMap="
					+ interactionMap + "]";
		}

		String actor;
		Map<String, List<BlockExp>> interactionMap;

		public Interaction(String actor) {
			this.actor = actor;
			interactionMap = new HashMap<>();
		}
	}
	
	List<Number> countForeignAttrCheckValues;
	@Override
	public void performAnalysis() {
		countForeignAttrCheckValues = new ArrayList<>();
		interactions = new HashMap<>();
		for (Scriptable scriptable : program.getAllScriptables()) {
			for (BlockExp sensorBlock : scriptable.allSensorBlocks()) {
				String theOtherScriptableName = sensorBlock.getOperand(1)
						.toString();
				if (!scriptable.getName().equals(theOtherScriptableName)) {
					Interaction interact;
					if(interactions.containsKey(scriptable)) {
						interact = interactions.get(scriptable);
					}else {
						interact = new Interaction(scriptable.getName());
					}

					if(!interact.interactionMap.containsKey(theOtherScriptableName)){
					interact.interactionMap.put(theOtherScriptableName,
							new ArrayList<BlockExp>());
					}
					interact.interactionMap.get(theOtherScriptableName).add(
							sensorBlock);
					if(!interactions.containsKey(scriptable)){
						interactions.put(scriptable, interact);
					}
				}
			}
		}

		// second pass discard those does not pass the threshold
		for (Scriptable scriptable : interactions.keySet()) {
			Interaction interact = interactions.get(scriptable);
			List<String> toDiscard = new ArrayList<>();
			for (String otherScriptable : interact.interactionMap.keySet()) {
				int countForeignAttrCheck = interact.interactionMap.get(otherScriptable).size();
				countForeignAttrCheckValues.add(countForeignAttrCheck);
				if (countForeignAttrCheck < threshold) {
					toDiscard.add(otherScriptable);
				}
			}
			
			for (String keyToDiscard : toDiscard) {
				interact.interactionMap.remove(keyToDiscard);
			}
		}
	}

	@Override
	protected void buildFullReport() {
		fullReport.put("name", SMELL_NAME);
		List<ReportItem> instances = new ArrayList<ReportItem>();
		for (Scriptable scriptable : interactions.keySet()) {
			Interaction scriptableInteractions = interactions.get(scriptable);

			for (String other : scriptableInteractions.interactionMap.keySet()) {
				HashSet<String> scriptWithSmells = new HashSet<>();
				for (BlockExp sensor : scriptableInteractions.interactionMap
						.get(other)) {
					scriptWithSmells.add(sensor.parentScript().render());
				}
				instances.add(new ReportItem(scriptable.getName(), other,
						scriptWithSmells));
			}
		}
		fullReport.put("instances", instances);

	}

	public class ReportItem {
		private String scriptable;
		private String intimacy_with;
		private HashSet<String> locations;

		ReportItem(String name, String relationshipWith, HashSet<String> scripts) {
			this.scriptable = name;
			this.intimacy_with = relationshipWith;
			this.locations = scripts;

		}
	}

	@Override
	protected void buildShortReport() {
		int count = 0;
		for (Scriptable scriptable : interactions.keySet()) {
			Interaction scriptableInteractions = interactions.get(scriptable);
			count += scriptableInteractions.interactionMap.keySet().size();
		}
		shortReport.put("name", "II");
		shortReport.put("count", count);
		shortReport.put("dist",  Util.toWeighedValues(countForeignAttrCheckValues));

	}

}
