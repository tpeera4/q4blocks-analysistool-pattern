package analysis.smell.cloneutils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import main.ScratchBlocksMapper;
import AST.BlockSeq;
import AST.ScratchBlock;

public class Fragment implements Comparable<Fragment> {
	private final static Logger logger = Logger.getLogger(Fragment.class
			.getName());
	private static final int minNodeNumForFragment = 8;
	public List<ScratchBlock> blocks;

	public Fragment() {
		logger.setLevel(Level.OFF);
		blocks = new ArrayList<>();
	}

	public Fragment(ScratchBlock subtree) {
		blocks = new ArrayList<>();
		add(subtree);
	}

	public static List<Fragment> getFragment(BlockSeq block, int size) {
		List<Fragment> fragments = new ArrayList<>();
		int start = 0;
		int end = block.getNumBlocks();
		while (start < end) {
			int back = start + size;
			if (back > end) {
				break;
			}
			Fragment fragment = new Fragment();
			for (int current = start; current < back; current++) {
				fragment.add(block.getBlocks(current));
			}

			if (fragment.getSize() >= minNodeNumForFragment) {
				fragments.add(fragment);
			}
			logger.log(Level.INFO, "fragment segment:" + back + "/" + end);
			start++;
		}
		return fragments;
	}

	private void add(ScratchBlock block) {
		blocks.add(block);
	}

	@Override
	public String toString() {
		return "Fragment(" + blocks.size() + ")[" + blocks + "]";
	}

	public NameCounter toNameCounter() {
		NameCounter counter = new NameCounter();
		for (ScratchBlock block : blocks) {
			counter.putAll(block.toNameCounter());
		}
		return counter;
	}

	public List<ScratchBlock> serialize() {
		List<ScratchBlock> serializedTree = new ArrayList<>();
		for (ScratchBlock block : blocks) {
			serializedTree.addAll(block.getPostOrderNodes());
		}
		return serializedTree;
	}

	public static int hashSequenceOfBlocks(List<ScratchBlock> serializedFragment) {
		int hash = 7;
		for (ScratchBlock block : serializedFragment) {
			int blockID = ScratchBlocksMapper.name2id.get(block.blockName());
			hash = hash * 31 + blockID;
		}

		return hash;
	}

	public int getHash() {
		return hashSequenceOfBlocks(serialize());
	}

	public int getSize() {
		return serialize().size();
	}

	public String render() {
		String result = "";
		for (ScratchBlock block : blocks) {
			result += block.render();
			result += "\n";
		}
		return result;
	}

	public int getEndPos() {
		List<ScratchBlock> lastSubtreeNodes = blocks.get(blocks.size() - 1)
				.getPostOrderNodes();
		int endPos = lastSubtreeNodes.get(lastSubtreeNodes.size() - 1)
				.getPositionInScript();
		return endPos;
	}

	public int getStartPos() {
		List<ScratchBlock> firstSubtreeNodes = blocks.get(0)
				.getPostOrderNodes();
		int startPos = firstSubtreeNodes.get(0).getPositionInScript();
		return startPos;
	}
	
	static Map<Fragment, Integer> cache = new HashMap<>();
	@Override
	public int compareTo(Fragment other) {
		Integer thisSize = null;
		Integer otherSize = null;
		if(cache.containsKey(this)){
			thisSize = cache.get(this);
		}else{
			thisSize = this.serialize().size();
			cache.put(this, thisSize);
		}
		if(cache.containsKey(other)){
			otherSize = cache.get(other);
		}else{
			otherSize = other.serialize().size();
			cache.put(other, otherSize );
			
		}
		
		return otherSize - thisSize;
	}
}
