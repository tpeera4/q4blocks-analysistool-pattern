package analysis.smell;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import AST.Sprite;
import analysis.Analysis;

public class BadNamingAnalysis extends Analysis {
	Set<Sprite> spriteWithBadNames; 
	@Override
	public void initialize() {
		spriteWithBadNames =  new HashSet<>();
	}

	@Override
	public void performAnalysis() {
		for(Sprite sprite: program.getStage().getSpriteList()){
			if(sprite.getName().contains("Sprite")){
				spriteWithBadNames.add(sprite);
			}
		}
	}

	@Override
	protected void buildFullReport() {
		fullReport.put("name", "Uncommunicative Name");
		List<ReportItem> instances = new ArrayList<ReportItem>();
		for(Sprite sprite: program.getStage().getSpriteList()){
			if(spriteWithBadNames.contains(sprite)){
				HashSet<String> instanceSet = new HashSet<>();
				instanceSet.add(sprite.getName());
				instances.add(new ReportItem(sprite.getName(), instanceSet));
			}
		}
		fullReport.put("scriptables", instances);
	}
	
	public static class ReportItem {
		private String scriptable;
		private boolean value;
		private HashSet<String> instances;

		private ReportItem(String name, HashSet<String> value) {
			this.scriptable = name;
			this.instances= value;
		}
	}

	@Override
	protected void buildShortReport() {
		shortReport.put("name", "UN");
		int numberOfSprites = program.getStage().getNumSprite();
		if(numberOfSprites==0){
			shortReport.put("percentage", null); 
		}else{
			shortReport.put("percentage", (double) spriteWithBadNames.size()*100/numberOfSprites);
		}
		shortReport.put("count", spriteWithBadNames.size());
	}



}
