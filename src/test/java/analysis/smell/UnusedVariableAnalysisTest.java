package analysis.smell;

import java.io.StringReader;

import com.google.gson.JsonObject;

import junit.framework.TestCase;
import main.ScratchParser;
import main.Util;
import AST.Program;
import analysis.smell.UnusedVariableAnalysis;

public class UnusedVariableAnalysisTest extends TestCase {
	private final static String FILENAME = "/smell/test-UnusedVar.json";
	private final static int projectID = 152433266;
	Program program = null;

	@Override
	protected void setUp() {
		StringReader srd = null;
		ScratchParser parser = new ScratchParser();
		program = null;

		try {
//			srd = Util.retrieveProjectOnline(projectID);
			 srd = Util.getStringReaderFromFile(FILENAME);
			program = parser.parse(srd);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void test() {
		UnusedVariableAnalysis analysis = new UnusedVariableAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		System.out.println(analysis.getFullReportAsJsonObject());
		JsonObject shortReportObj = analysis.getShortReportAsJsonObject();
		System.out.println(analysis.getShortReportAsJson());
		assertEquals(100*2.0/3.0, shortReportObj.get("percentage").getAsDouble());
	}
}