package analysis;

import java.io.StringReader;

import junit.framework.TestCase;
import main.ScratchParser;
import main.Util;
import AST.ASTNode;
import AST.Program;
import AST.ScratchBlock;
import AST.Sprite;
import analysis.ProgramStatisticAnalysis;

public class ProgramStatisticsTest extends TestCase {
	private final static String FILENAME = "/smell/test-stats.json";
//	private final static int projectID = 151770183;
	Program program = null;
	
	@Override
	protected void setUp() {
		StringReader srd = null;
		  ScratchParser parser = new ScratchParser();
	      program = null;
	      
	      try {
//	    	  srd = Util.retrieveProjectOnline(projectID);
	    	  srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}

	public void test() {
		Sprite sprite0 = program.getStage().getSprite(0);
		assertEquals(11,sprite0.allBlocks().size());
		assertEquals(2,program.getStage().allBlocks().size());
		Sprite sprite1 = program.getStage().getSprite(1);
		assertEquals(1,sprite1.allBlocks().size());
		assertEquals(14, program.getTotalNumberOfBlocks());
		assertEquals(4, program.getTotalNumberOfScript());
		assertEquals(8,program.getTotalNumberOfLiteral());
		assertEquals(1.5, program.getAverageScriptLength());
		assertEquals(1, program.getTotalNumberOfProcedure());
		assertEquals(1, program.getTotalNumberOfVariables());
		assertEquals(2, program.allComments().size());
		
		
	}

	public void testStats(){
		ProgramStatisticAnalysis analysis = new ProgramStatisticAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		String result = analysis.getShortReportAsJson();
		System.out.println(result);
	}
	

	
}