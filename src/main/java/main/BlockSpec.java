package main;

public class BlockSpec {
	String blockName;
	String blockType;
	String format;
	String[] argTypes;
	private int id;
	public String getBlockName() {
		return blockName;
	}
	public void setBlockName(String blockName) {
		this.blockName = blockName;
	}
	public String getBlockType() {
		return blockType;
	}
	public void setBlockType(String blockType) {
		this.blockType = blockType;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String[] getArgTypes() {
		return argTypes;
	}
	public void setArgTypes(String[] argTypes) {
		this.argTypes = argTypes;
	}
	public void setID(int i) {
		this.id = i;
	}
	public int getID() {
		return this.id;
	}
	
}
