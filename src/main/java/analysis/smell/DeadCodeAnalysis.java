package analysis.smell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import AST.Script;
import AST.Scriptable;
import AST.WhenIReceiveBlock;
import analysis.Analysis;
import analysis.ReportItem;

public class DeadCodeAnalysis extends Analysis {

	private Map<Scriptable, HashSet<Script>> scriptableToDeadCode;

	@Override
	public void initialize() {
		scriptableToDeadCode = new HashMap<>();
	}

	@Override
	public void performAnalysis() {
		for (WhenIReceiveBlock receiverBlock : program.allMessageReceivers()) {
			if (receiverBlock.getSenders().isEmpty()) {
				
				if (!scriptableToDeadCode.containsKey(receiverBlock
						.spriteParent())) {
					scriptableToDeadCode.put(receiverBlock.spriteParent(), new HashSet<Script>());
				}
				scriptableToDeadCode.get(receiverBlock.spriteParent()).add(
						receiverBlock.parentScript());
			}
		}
	}

	@Override
	protected void buildFullReport() {
		fullReport.put("name", "Unreachable Code");
		List<ReportItem> instances = new ArrayList<ReportItem>();

		for (Scriptable scriptable : scriptableToDeadCode.keySet()) {
			HashSet<Script> deadCodes = scriptableToDeadCode.get(scriptable);
			HashSet<String> deadCodesAsString = new HashSet<>();
			for(Script deadCode : deadCodes){
				deadCodesAsString.add(deadCode.render());
			}
			instances.add(new ReportItem(scriptable.getName(), deadCodesAsString));
		}
		fullReport.put("scriptables", instances);
	}

	@Override
	protected void buildShortReport() {
		shortReport.put("name", "UC");
		int totalBlocks = program.getTotalNumberOfBlocks();
		float numKBlocks = (float) totalBlocks / 100;
		int instanceCount = 0;
		
		for (Scriptable scriptable : scriptableToDeadCode.keySet()) {
			HashSet<Script> deadCodes = scriptableToDeadCode.get(scriptable);
			instanceCount += deadCodes.size();
		}
		if(numKBlocks==0){
			shortReport.put("density", null);
			shortReport.put("count", null);
		}else{
			shortReport.put("density", (double) instanceCount / numKBlocks);
			shortReport.put("count", (double) instanceCount);
		}
		
	}

}
