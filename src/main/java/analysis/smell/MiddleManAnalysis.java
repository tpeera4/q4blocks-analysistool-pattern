package analysis.smell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import AST.BroadcastBlock;
import AST.MessageBlock;
import AST.Scriptable;
import analysis.Analysis;
import main.Util;

public class MiddleManAnalysis extends Analysis {

	public static final String SMELL_NAME = "Middle Man";
	public List<List<MessageBlock>> middleManPath;
	public Map<Scriptable, List<List<MessageBlock>>> middleManPathMap = new HashMap<>();

	@Override
	public void initialize() {
	}

	List<Number> chainLengthDist = new ArrayList<>();

	int count = 0;

	@Override
	public void performAnalysis() {
		middleManPath = new ArrayList<>();
		for (MessageBlock senderBlock : program.allMessageSenders()) {
			Set<MessageBlock> reachableReceiverBlocks = senderBlock.reachable();
			for (MessageBlock receiverBlock : reachableReceiverBlocks) {
				List<MessageBlock> path = BFS(senderBlock, receiverBlock);
				if (hasMiddleManSmell(path)) {
					chainLengthDist.add(path.size() / 2);

					middleManPath.add(path);
					Scriptable spriteOrigin = path.get(0).spriteParent();
					if (!middleManPathMap.containsKey(spriteOrigin)) {
						middleManPathMap.put(spriteOrigin, new ArrayList<List<MessageBlock>>());
					}
					middleManPathMap.get(spriteOrigin).add(path);
					count++;
				}
			}
		}
	}

	private boolean hasMiddleManSmell(List<MessageBlock> path) {
		boolean isSmell = false;
		// only greater than 2 that MM smell can occur
		// if sender in the middle located in the script with just a
		// receiver and the sender itself
		if (path.size() > 2) {
			// assume that all script sender/receiver in the
			// middle has only two blocks
			isSmell = true;
			for (int i = 1; i < path.size() - 1; i++) {
				int loc = path.get(i).parentScript().getLineOfCode();
				// need just one script loc > 2 to make smell invalid
				if (loc != 2) {
					isSmell = false;
				}
			}
		}
		return isSmell;
	}

	List<MessageBlock> BFS(MessageBlock sender, MessageBlock targetSearch) {
		Map<MessageBlock, Boolean> visited = new HashMap<>();

		// Create a queue for BFS
		LinkedList<MessageBlock> queue = new LinkedList<MessageBlock>();
		LinkedList<List<MessageBlock>> pathQueue = new LinkedList<>();

		// setup
		// Mark the current node as visited and enqueue it
		visited.put(sender, true);
		queue.add(sender);
		List<MessageBlock> firstPath = new ArrayList<MessageBlock>();
		firstPath.add(sender);
		pathQueue.add(firstPath);

		while (queue.size() != 0) {
			// Dequeue a vertex from queue and print it
			sender = queue.poll();
			List<MessageBlock> path = pathQueue.poll();
			sender = path.get(path.size() - 1);

			Iterator<MessageBlock> receiverIter = sender.getReceivers().iterator();
			while (receiverIter.hasNext()) {
				MessageBlock receiver = receiverIter.next();
				// also add broadcast within same script to the queue
				HashSet<BroadcastBlock> messageSenders = receiver.parentScript().messageSenders();
				path.add(receiver);
				if (receiver.equals(targetSearch)) {
					return path;
				}

				queue.addAll(messageSenders);
				for (BroadcastBlock senderBlock : messageSenders) {
					ArrayList<MessageBlock> newPath = new ArrayList<>(path);
					newPath.add(senderBlock);
					pathQueue.add(newPath);
				}
				// fix : messageSenders should output list to preserve ordering
			}
			visited.put(sender, true);
		}
		return new ArrayList<>();
	}

	@Override
	protected void buildFullReport() {
		fullReport.put("name", SMELL_NAME);
		List<ReportItem> instances = new ArrayList<ReportItem>();
		for (Scriptable scriptable : middleManPathMap.keySet()) {
			List<List<MessageBlock>> paths = middleManPathMap.get(scriptable);
			for (List<MessageBlock> path : paths) {
				Set<InstanceItem> msgChainElement = new LinkedHashSet<>();
				for (MessageBlock msgBlock : path) {
					msgChainElement.add(new InstanceItem(msgBlock.spriteParent().getName(),
							msgBlock.spriteParent().getCostume(0).getValue(), msgBlock.parentScript().render()));
				}
				ReportItem item = new ReportItem(msgChainElement);
				instances.add(item);
			}
		}
		fullReport.put("instances", instances);
	}

	public class ReportItem {
		public List<InstanceItem> instance;

		public ReportItem() {
		}

		public ReportItem(Set<InstanceItem> msgChainElements) {
			instance = new ArrayList<>(msgChainElements);
		}
	}

	class InstanceItem {
		String scriptable;
		String location;
		String costume;

		public InstanceItem(String scriptable, String costume, String location) {
			this.scriptable = scriptable;
			this.costume = costume;
			this.location = location;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (!(this instanceof InstanceItem))
				return false;
			InstanceItem that = (InstanceItem) o;
			return this.location.equals(that.location);
		}

		@Override
		public int hashCode() {
			int result = 17;
			result = 37 * result + scriptable.hashCode();
			result = 37 * result + location.hashCode();
			return result;
		}

	}

	@Override
	protected void buildShortReport() {
		shortReport.put("name", "MM");
		shortReport.put("count", count);
		shortReport.put("chain_length_dist", Util.toWeighedValues(chainLengthDist));
	}

}
