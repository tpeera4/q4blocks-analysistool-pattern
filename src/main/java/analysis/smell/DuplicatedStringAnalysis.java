package analysis.smell;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.JsonObject;

import main.Util;
import AST.Scriptable;
import AST.StringLiteral;
import analysis.Analysis;
import analysis.smell.InappropriateIntimacyAnalysis.ReportItem;

public class DuplicatedStringAnalysis extends Analysis {
	private static final int groupSizeThreshold = 4;
	public static final String SMELL_NAME = "Duplicated String";
	Map<String, List<StringLiteral>> stringUsageMap = new HashMap<>();
	private int thresholdValue = 5;
	int totalNumOfUniqueStrings = 0;

	@Override
	public void initialize() {
		// TODO Auto-generated method stub

	}

	int count = 0;
	int totalStringLiteral = 0;
	private List<Number> lengthDis;
	private List<Number> groupSize;

	@Override
	public void performAnalysis() {
		List<StringLiteral> stringLitList = new ArrayList<>(program.allStringLiterals());
		List<String> considered = Arrays
				.asList(new String[] { "say:duration:elapsed:from:", "say:", "think:duration:elapsed:from:", "think:",
						"doAsk", "concatenate:with:", "letter:of:", "stringLength:" });
		for (StringLiteral lit : stringLitList) {
			if (considered.contains(lit.blockParent().blockName())) {
				totalStringLiteral++;
				if (!stringUsageMap.containsKey(lit.getValue())) {
					stringUsageMap.put(lit.getValue(), new ArrayList<StringLiteral>());
				}
				stringUsageMap.get(lit.getValue()).add(lit);
			}
		}

		List<String> toRemove = new ArrayList<>();

		lengthDis = new ArrayList<Number>();
		groupSize = new ArrayList<Number>();
		for (String key : stringUsageMap.keySet()) {
			if (stringUsageMap.get(key).size() < groupSizeThreshold) {
				toRemove.add(key);
			} else {
				count++;
			}

			if (stringUsageMap.get(key).size() > 1) {
				lengthDis.add(key.length());
				groupSize.add(stringUsageMap.get(key).size());
			}
		}
		totalNumOfUniqueStrings = stringUsageMap.size();
		for (String key : toRemove) {
			stringUsageMap.remove(key);
		}
	}

	/*
	 * @see smellanalysis.Analysis#buildFullReport() [ { instance: "My name is",
	 * locations: { scriptable: "sprite1", locations : [ SayBlock blah, ThinkBlock
	 * blah]} } ]
	 */
	class _InstanceItem {
		public String instanceKey;
		public List<InstanceItem> instances = new ArrayList<InstanceItem>();
		public Map<String, List<String>> spriteToInstanceValues = new HashMap<>();

		@Override
		public String toString() {
			return "ReportItem [instanceKey=" + instanceKey + ", spriteToInstances=" + spriteToInstanceValues + "]";
		}
	}
	
	class InstanceItem {
		public String dup_string;
		public List<InstanceLocation> scriptables = new ArrayList<InstanceLocation>();
	}
	
	class InstanceLocation {
		public String scriptable;
		public List<String> locations;
	}

	@Override
	protected void buildFullReport() {
		fullReport.put("name", SMELL_NAME);
		List<InstanceItem> instances = new ArrayList<InstanceItem>();

		// preprocessing
		Map<String, _InstanceItem> dupStringKeyToLocInstances = new HashMap<>();
		for (String dupStringKey : stringUsageMap.keySet()) {
			_InstanceItem item;
			for (StringLiteral lit : stringUsageMap.get(dupStringKey)) {
				if (dupStringKeyToLocInstances.containsKey(dupStringKey)) {
					item = dupStringKeyToLocInstances.get(dupStringKey);
				} else {
					item = new _InstanceItem();
				}
				item.instanceKey = dupStringKey;
				String spriteName = lit.spriteParent().getName();
				if (!item.spriteToInstanceValues.containsKey(spriteName)) {
					item.spriteToInstanceValues.put(spriteName, new ArrayList<String>());
				}
				item.spriteToInstanceValues.get(spriteName).add(lit.blockParent().render());
				dupStringKeyToLocInstances.put(dupStringKey, item);
			}
		}
		
		// post processing of the report
		for(_InstanceItem reportItem : dupStringKeyToLocInstances.values()) {
			InstanceItem instItem= new InstanceItem();
			Set<String> spriteKeys = reportItem.spriteToInstanceValues.keySet();
			instItem.dup_string= reportItem.instanceKey;
			for(String spriteKey: spriteKeys) {
				InstanceLocation scriptableLoc = new InstanceLocation();
				scriptableLoc.scriptable = spriteKey;
				scriptableLoc.locations = reportItem.spriteToInstanceValues.get(spriteKey);
				instItem.scriptables.add(scriptableLoc);
			}
			instances.add(instItem);
		}
		fullReport.put("instances", instances);
	}

	@Override
	protected void buildShortReport() {
		shortReport.put("name", "DS");
		if (totalStringLiteral == 0) {
			shortReport.put("percentage", null);
			shortReport.put("count", null);
		} else {
			shortReport.put("percentage", (double) count * 100 / totalNumOfUniqueStrings);
			shortReport.put("count", (double) count);
		}
		shortReport.put("length_dist", Util.toWeighedValues(lengthDis));
		shortReport.put("group_size_dist", Util.toWeighedValues(groupSize));
	}

}
