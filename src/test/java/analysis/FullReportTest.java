package analysis;

import java.io.StringReader;

import AST.Costume;
import AST.Program;
import junit.framework.TestCase;
import main.AnalysisManager;
import main.ScratchParser;
import main.Util;

public class FullReportTest extends TestCase {
	private final static String FILENAME = "/test-input.txt";
	private final static int projectID = 149974792;
	Program program = null;
	
	@Override
	protected void setUp() {
		StringReader srd = null;
		  ScratchParser parser = new ScratchParser();
	      program = null;
	      
	      try {
	    	  srd = Util.retrieveProjectOnline(projectID);
//	          srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}

	public void testParseCostumeInfo() {
		Costume costume = program.getStage().getSprite(0).getCostume(0);
		assertNotNull(costume.getValue());
	}
	
	public void testFullAnalysisReportHasCostumeInfo() {
		String fullJsonReport = AnalysisManager.process(program);
		System.out.println(fullJsonReport);
	}
	
	
}