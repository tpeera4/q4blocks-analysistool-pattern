package analysis.smell;

import java.io.FileNotFoundException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;
import main.ScratchParser;
import main.Util;
import AST.BlockSeq;
import AST.CommandBlock;
import AST.Program;
import AST.ScratchBlock;
import AST.Script;
import AST.Scriptable;
import AST.Sprite;
import analysis.smell.DuplicatedCodeAnalysis;
import analysis.smell.cloneutils.Fragment;
import analysis.smell.cloneutils.FragmentLSH;
import analysis.smell.cloneutils.NameCounter;

public class DuplicatedCodeAnalysisTest extends TestCase {
	private final static String FILENAME = "/smell/test-DuplicatedCode.json";
	Program program = null;

	@Override
	protected void setUp() throws Exception {
		StringReader srd = null;
		ScratchParser parser = new ScratchParser();
		DuplicatedCodeAnalysis.minWindowSize = 1;
		DuplicatedCodeAnalysis.maxWindowSize = 10;
		DuplicatedCodeAnalysis.minimumSerializedSubtreeSize = 8;
		program = null;

		srd = Util.getStringReaderFromFile(FILENAME);
		// srd = Util.retrieveProjectOnline(152433266);
		program = parser.parse(srd);
	}

	public void testGetAllFragmentsForEachScriptable() {
		Sprite sprite = program.getStage().getSprite(0);
		HashSet<BlockSeq> blockSequences = sprite.allBlockSeq();
		System.out.println(blockSequences);
		assertEquals(3, blockSequences.size());
	}

	public void testGetAllSubtrees() {
		Sprite sprite = program.getStage().getSprite(4);
		HashSet<ScratchBlock> subtrees = sprite.allSubtrees();
		System.out.println(subtrees);
	}

	public void testVectorGeneration() {
		Sprite sprite = program.getStage().getSprite(0);
		Script script = sprite.getScript(0);
		NameCounter vec = script.getBody().getBlocks(1).toNameCounter();
		assertEquals(2, vec.nodeCount); // move 10 steps
	}

	public void testVectorOfSubTree() {
		Sprite sprite = program.getStage().getSprite(0);
		Script script = sprite.getScript(0);
		ScratchBlock block = script.getBody().getBlocks(0);

	}

	public void testSlidingWindow() {
		Sprite sprite = program.getStage().getSprite(1);
		Script script = sprite.getScript(0);
		BlockSeq block = script.getBody();

		List<Fragment> sizeOnefragments = Fragment.getFragment(block, 1);
		assertEquals(0, sizeOnefragments.size());

		List<Fragment> sizeTwofragments = Fragment.getFragment(block, 2);
		assertEquals(0, sizeTwofragments.size());

		List<Fragment> sizeThreefragments = Fragment.getFragment(block, 3);
		assertEquals(0, sizeThreefragments.size());
	}

	public void testVectorOfFragment() {
		Sprite sprite = program.getStage().getSprite(3);
		Script script = sprite.getScript(0);
		BlockSeq blockSeq = script.getBody();
		List<Fragment> sizeOnefragments = Fragment.getFragment(blockSeq, 1);
		Fragment fragment = sizeOnefragments.get(0);
		ScratchBlock firstBlock = blockSeq.getBlocks(0);
		assertTrue(Arrays.equals(firstBlock.toNameCounter().getVector(), fragment.toNameCounter().getVector()));

	}

	public void testDuplicatedFragments() {
		Sprite sprite = program.getStage().getSprite(2);
		FragmentLSH lsh = new FragmentLSH();

		for (Script script : sprite.getScriptList()) {
			BlockSeq block = script.getBody();
			List<Fragment> sizeThreefragments = Fragment.getFragment(block, 3);
			lsh.hashFragments(sizeThreefragments);
		}

		Map<Integer, Set<Fragment>> cloneGroups = lsh.getBucketsOfCandidateClones();
		for (Integer groupNum : cloneGroups.keySet()) {
			System.out.println(cloneGroups.get(groupNum));
		}

	}

	public void testDuplicatedFragmentContainingCustomBlocks() {
		Sprite sprite = program.getStage().getSprite(8);
		FragmentLSH lsh = new FragmentLSH();

		for (Script script : sprite.getScriptList()) {
			BlockSeq block = script.getBody();
			List<Fragment> sizeThreefragments = Fragment.getFragment(block, 3);
			lsh.hashFragments(sizeThreefragments);
		}
	}

	public void testDuplicatedSubtrees() {
		Sprite sprite = program.getStage().getSprite(3);
		FragmentLSH lsh = new FragmentLSH();

		HashSet<ScratchBlock> subtrees = sprite.allSubtrees();
		List<Fragment> subtreeFragments = new ArrayList<>();

		for (ScratchBlock subtree : subtrees) {
			Fragment fragment = new Fragment(subtree);
			subtreeFragments.add(fragment);
		}
		lsh.hashFragments(subtreeFragments);

		Map<Integer, Set<Fragment>> candiateCloneBuckets = lsh.getBucketsOfCandidateClones();

		for (Integer groupNum : candiateCloneBuckets.keySet()) {
			System.out.println(candiateCloneBuckets.get(groupNum));
		}
	}

	public void testCheckWithinSubtreeCloneGroup() {
		Sprite sprite = program.getStage().getSprite(5);

		Map<Integer, Set<Fragment>> candidates = DuplicatedCodeAnalysis.analyzeForSubtreeCloneCandidate(sprite);

		Map<Integer, Set<Fragment>> finalCloneGroup = DuplicatedCodeAnalysis.checkCandidateClones(candidates);
		System.out.println(finalCloneGroup.size());
	}

	public void testCheckWithinFragmentCloneGroup() {
		Sprite sprite = program.getStage().getSprite(5);

		Map<Integer, Set<Fragment>> candidates = DuplicatedCodeAnalysis.analyzeForForestCloneCandidate(sprite);

		Map<Integer, Set<Fragment>> finalCloneGroup = DuplicatedCodeAnalysis.checkCandidateClones(candidates);
		assertEquals(1, finalCloneGroup.size());
		System.out.println(finalCloneGroup.size());
	}

	public void testBothSubtreeAndForest() {
		Sprite sprite = program.getStage().getSprite(5);

		List<Set<Fragment>> candidates = DuplicatedCodeAnalysis.analyzeForClones(sprite);

		List<Set<Fragment>> finalCloneGroup = DuplicatedCodeAnalysis.postProcess(candidates);
		assertEquals(1, finalCloneGroup.size());
	}

	public void testDuplicatedCodeAnalyzer() {
		DuplicatedCodeAnalysis analysis = new DuplicatedCodeAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		String report = analysis.getFullReportAsJson();
		System.out.println(report);
	}

	public void testShortReport() {
		DuplicatedCodeAnalysis analysis = new DuplicatedCodeAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		String report = analysis.getShortReportAsJson();
		System.out.println(report);
	}

	public void testMinimumNodeInSubtree() {
		Sprite sprite = program.getStage().getSprite(7);

		List<Set<Fragment>> candidates = DuplicatedCodeAnalysis.analyzeForClones(sprite);
		assertTrue(candidates.size() > 0); // minimum 8 nodes
	}

}