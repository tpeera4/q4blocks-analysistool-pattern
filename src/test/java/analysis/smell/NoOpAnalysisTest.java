package analysis.smell;

import java.io.StringReader;

import com.google.gson.JsonObject;

import junit.framework.TestCase;
import main.ScratchParser;
import main.Util;
import AST.Program;
import analysis.smell.NoOpAnalysis;

public class NoOpAnalysisTest extends TestCase {
	private final static String FILENAME = "/smell/test-NoOp.json";
//	private final static int projectID = 151770183;
	Program program = null;
	
	@Override
	protected void setUp() {
		StringReader srd = null;
		  ScratchParser parser = new ScratchParser();
	      program = null;
	      
	      try {
//	    	  srd = Util.retrieveProjectOnline(151770183);
	          srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}

	public void test() {
		NoOpAnalysis analysis = new NoOpAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		String fullJsonReport = analysis.getFullReportAsJson();
		System.out.println(fullJsonReport);
		String shortReport = analysis.getShortReportAsJson();
		System.out.println(shortReport);
		JsonObject shortJsonReport = analysis.getShortReportAsJsonObject();
		assertEquals(2, shortJsonReport.get("count").getAsInt());
	}
}