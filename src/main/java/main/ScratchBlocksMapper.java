package main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import analysis.smell.cloneutils.RelevantASTNode;

public class ScratchBlocksMapper {
  private static Map<String, BlockSpec> blockMap = new HashMap<String, BlockSpec>();
  public final static Map<String, Integer> name2id = new HashMap<String, Integer>();
  public final static Map<Integer, String> id2name = new HashMap<Integer, String>();
  static int ID = 0;
  static { 
 	String JSON_PATH = "/block-specs.json";
  	BufferedReader br = null;
  	
  	URL url = Main.class.getResource(JSON_PATH);
 

  	try{
  		InputStreamReader dataReader = new InputStreamReader(url.openStream());
  		br = new BufferedReader(dataReader);		
  	}catch(Exception e){
  		e.printStackTrace();
  	}
  	
   	JsonParser parser = new JsonParser();
	JsonElement jsonTree = parser.parse(br);
	JsonArray array = jsonTree.getAsJsonArray();

	for(int i=0; i<array.size();i++){
		JsonArray specArray = array.get(i).getAsJsonArray();
		if(specArray.size()<4){
			continue;
		}
		BlockSpec spec = new BlockSpec();
		spec.setFormat(reformat(specArray.get(0).getAsString()));
		spec.setBlockType(fullBlockTypeOf(specArray.get(1).getAsString()));
		spec.setBlockName(specArray.get(3).getAsString());
		spec.setArgTypes(extractArgTypes(specArray.get(0).getAsString()));
		blockMap.put(specArray.get(3).getAsString(), spec);
	}
	
	//assign Name to ID
	SortedSet<String> keys = new TreeSet<String>(blockMap.keySet());
	for(String astNodeName : RelevantASTNode.relevantASTNodeNames){
		keys.add(astNodeName);
	}
	
	for(String key: keys){
		name2id.put(key, ID);
		id2name.put(ID, key);
		ID++;
	}
  }
  
  public static int getIDFromName(String name){
	  if(name2id.containsKey(name)){
		  return name2id.get(name);
	  }else{
		  id2name.put(ID, name);
		  name2id.put(name, ID);
		  ID++;
		  return name2id.get(name);
	  }
  }

  public static void main(String[] args){
  		System.out.println(blockMap);
  		System.out.println(id2name);
  		for(Integer id: id2name.keySet()){
  			System.out.println(name2id.get(id2name.get(id)));
  		}
  		System.out.println(name2id);
  }

  public static String getFormatOf(String scratchBlockName){
	  if(blockMap.containsKey(scratchBlockName)){
		  return blockMap.get(scratchBlockName).getFormat();
	  }else{
		  return reformat(scratchBlockName);	//undefined
	  }
  }

  public static String getBlockType(String scratchBlockName){
	  if(blockMap.containsKey(scratchBlockName)){
		  return blockMap.get(scratchBlockName).getBlockType();
	  }else{
		  return "undefined";
	  }
  }

  public static String fullBlockTypeOf(String blockType){
  	String fullType = "";
  	switch(blockType){
  		case "b": fullType = "Boolean";
  				break;
  		case "r": fullType = "Reporter";
  				break;
  		default : fullType = "stack";
  				break;
  	}
  	return fullType;
  }

  public static String[] getArgTypes(String scratchBlockName){
	  if(blockMap.containsKey(scratchBlockName)){
		  return blockMap.get(scratchBlockName).getArgTypes();
	  }else{ //undefined
		  return extractArgTypes(scratchBlockName);
	  }
  }


  public static String[] extractArgTypes(String asString) {
		Pattern p = Pattern.compile("\\%(Z|n|b|d|s|c|m.(\\w)+|x.(\\w)+)");
		Matcher m = p.matcher(asString);
		List<String> types = new ArrayList<String>();
		String type;
		while (m.find()) {
			switch (m.group().substring(0, 2)) { //first two character %X
				case "%b": 	type = "Boolean";
							break;
				case "%c": 	type = "Color";
							break;
				case "%d": 	type = "Number-menu";
							break;
				case "%m": 	type = "Readonly-menu";
							break;
				case "%n": 	type = "Number";
							break;
				case "%s": 	type = "String";
							break;
				case "%x": 	type = "Inline";
							break;
				case "%Z": 	type = "Block";
							break;
				  default:	type = "String";
				  			break;
			}
			types.add(type);
		}
		String[] typeOrdering = types.toArray(new String[types.size()]);
		return typeOrdering;
	}

	public static String reformat(String format) {
		String reformat = format.replaceAll("\\%(Z|n|b|d.(\\w)+|c|m.(\\w)+|x.(\\w)+)", "%s");
		return reformat;
	}
	
	public static String customBlockReformat(String format){
		String reformat = format.replaceAll("\\%(Z|n|d|c|m.(\\w)+|x.(\\w)+)", "%s");
		reformat = reformat.replaceAll("\\%b", "\\\\ %s");
		return reformat;
	}
	
	

  
}
