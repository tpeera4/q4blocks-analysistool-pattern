package analysis;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import AST.Program;

public abstract class Analysis {
	protected Program program;
	protected Map<String, Object> fullReport;
	protected Map<String, Object> shortReport;
	Gson gson = new GsonBuilder().disableHtmlEscaping().serializeNulls().create();
	
	public Analysis(){
		fullReport = new HashMap<String, Object>();
		shortReport = new HashMap<String, Object>();
		initialize();
	}
	
	public abstract void initialize();

	public void setInputProgram(Program program) {
		this.program = program;
	}

	/*
	 * Using simple analysis representation as possible
	 * And keep the reference to the instance of the smell itself
	 * We con recover the information about script and sprite 
	 * if they are children of those in the tree 
	 */
	public abstract void performAnalysis();
	
	
	/*
	 * { name: smell_name,
	 * 	 scriptables: [
	 * 		 {  
	 * 			name: scriptable_name,
	 * 			instances: [ 
	 * 					first_smell_instance,
	 * 					second_smell_instance
	 * 				]			
	 *		 }
	 * 	   ]
	 * }
	 */
	
	public String getFullReportAsJson() {
		
		buildFullReport();
		String json = gson.toJson(fullReport);
		return json;
	}
	
	public JsonObject getFullReportAsJsonObject(){
		buildFullReport();
		return gson.toJsonTree(fullReport).getAsJsonObject();
	}
	
	public JsonObject getShortReportAsJsonObject(){
		buildShortReport();
		return gson.toJsonTree(shortReport).getAsJsonObject();
	}
	
	public String getShortReportAsJson(){
		shortReport = new HashMap<String, Object>();
		buildShortReport();
		String json = gson.toJson(shortReport);
		return json;
	}
	
	/*
	 * Build the report from the analysis representation
	 */
	protected abstract void buildFullReport();

	/*
	 * a simple int counter, Stat instance object instrumented
	 * in the performaAnalysis() routine can be used here
	 */
	protected abstract void buildShortReport() ;

}
