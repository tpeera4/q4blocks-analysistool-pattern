package main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import AST.*;

public class Main {
    private final static String FILENAME = "/parsing/test-real-01.json"; //path in the jar

    public static void main(String[] args) {
      StringReader srd = null;
      BufferedReader rd = null;
      Program program = null;
      
      
      try {
        URL url = Main.class.getResource(FILENAME);
        InputStreamReader dataReader = new InputStreamReader(url.openStream());
        rd = new BufferedReader(dataReader);

        String inputLine = null;
        StringBuilder builder = new StringBuilder();

        while((inputLine = rd.readLine()) != null)
          builder.append(inputLine);

//          srd = new StringReader(builder.toString());
        srd = Util.retrieveProjectOnline(151770183);
        ScratchParser parser = new ScratchParser();
        program = parser.parse(srd);
//        System.out.println("Syntax is okay");

      } catch (Throwable e) {
          // Catching Throwable is ugly but JavaCC throws Error objects!
        System.out.println("Syntax check failed: " + e.getMessage());
        e.printStackTrace();
      }
      
      String json = AnalysisManager.process(program);
    // String json = AnalysisManager.processForShortReport(program);
     System.out.println(json);

//		 DuplicatedCodeAnalysis analysis = new DuplicatedCodeAnalysis();
//		DuplicatedStringAnalysis analysis = new DuplicatedStringAnalysis(); 
//      	analysis.setInputProgram(program);
//		 analysis.performAnalysis();
//		 String report = analysis.getShortReportAsJson();
//		 System.out.println(report);

    // CFG
      Sprite sprite = program.getStage().getSprite(0);
      Script script = sprite.getScript(0); 
      ASTNode blockSeq = script.getChild(0);
      ASTNode list = blockSeq.getChild(0);
      ASTNode block = list.getChild(0);
      CommandBlock firstBlock = (CommandBlock) script.getBody().getBlocks(0);
      System.out.println(script.CFGDotSpec());


    }
  }
