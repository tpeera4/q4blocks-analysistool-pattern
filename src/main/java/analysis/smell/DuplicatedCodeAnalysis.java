package analysis.smell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.Util;
import AST.BlockSeq;
import AST.ScratchBlock;
import AST.Script;
import AST.Scriptable;
import analysis.Analysis;
import analysis.smell.cloneutils.Fragment;
import analysis.smell.cloneutils.FragmentLSH;

public class DuplicatedCodeAnalysis extends Analysis {
	public static int minWindowSize = 1;
	public static int maxWindowSize = 10; //10
	public static int minimumSerializedSubtreeSize = 8;
	public static final int subTreeSizeToConsider = 4;
	
	private final static Logger logger = Logger
			.getLogger(DuplicatedCodeAnalysis.class.getName());
	
	public void setAnalysisParameters(int minWin, int maxWin, int minTreeNodes){
		minWindowSize = minWin;
		maxWindowSize = maxWin;
		minimumSerializedSubtreeSize = minTreeNodes;
	}

	@Override
	public void initialize() {
		logger.setLevel(Level.OFF);
	}

	@Override
	public void performAnalysis() {
		scriptableToCloneGroups = new HashMap<>();

		for (Scriptable scriptable : program.getAllScriptables()) {
			List<Set<Fragment>> candidates = analyzeForClones(scriptable);
			logger.log(Level.INFO, "Considered " + candidates.size()
					+ " candidates for" + scriptable.getName());
			List<Set<Fragment>> finalCloneGroups = postProcess(candidates);
			if (finalCloneGroups.isEmpty()) {
				continue;
			}
			if (!scriptableToCloneGroups.containsKey(scriptable)) {
				scriptableToCloneGroups.put(scriptable,
						new ArrayList<Set<Fragment>>());
			}
			scriptableToCloneGroups.get(scriptable).addAll(finalCloneGroups);
		}
	}

	public static Map<Integer, Set<Fragment>> analyzeForForestCloneCandidate(
			Scriptable scriptable) {
		FragmentLSH lsh = new FragmentLSH();
		HashSet<BlockSeq> allBlockSequences = scriptable.allBlockSeq();
		List<Fragment> fragments = new ArrayList<>();

		for (int fragmentSize = minWindowSize; fragmentSize < maxWindowSize; fragmentSize++) {
			logger.log(Level.INFO, "current window size:" + fragmentSize);
			for (BlockSeq blockSeq : allBlockSequences) {
				fragments.addAll(Fragment.getFragment(blockSeq, fragmentSize));
			}
		}
		logger.log(Level.INFO, "LSH hash in total of: " + fragments.size()
				+ " fragments");
		lsh.hashFragments(fragments);
		return lsh.getBucketsOfCandidateClones();
	}

	public static Map<Integer, Set<Fragment>> analyzeForSubtreeCloneCandidate(
			Scriptable scriptable) {
		FragmentLSH lsh = new FragmentLSH();
		HashSet<ScratchBlock> subtrees = scriptable.allSubtrees();
		logger.log(Level.INFO, "All subtrees:" + subtrees.size());
		List<Fragment> subtreeFragments = new ArrayList<>();

		for (ScratchBlock subtree : subtrees) {
			Fragment fragment = new Fragment(subtree);
			if(fragment.serialize().size()>= minimumSerializedSubtreeSize){
				subtreeFragments.add(fragment);
			};
		}
		logger.log(Level.INFO,
				"LSH hash in total of:" + subtreeFragments.size()
						+ " fragments");
		lsh.hashFragments(subtreeFragments);

		return lsh.getBucketsOfCandidateClones();
	}

	public static List<Set<Fragment>> postProcess(
			List<Set<Fragment>> candidateBuckets) {
		logger.log(Level.INFO, "Post processing");
		Map<Integer, Set<Fragment>> orderingSensitiveCloneGroupCandidates = new HashMap<>();
		Map<Script, Set<Fragment>> scriptToCloneCandidateSet = new HashMap<>();

		int bucketIdx = 0;
		for (Set<Fragment> bucket : candidateBuckets) {
			logger.log(Level.INFO, "bucket " + bucketIdx++);
			Map<Integer, Set<Fragment>> subBuckets = hashIntoOrderingSensitiveBucket(bucket);
			for (Integer bucketHashKey : subBuckets.keySet()) {
				Set<Fragment> subBucket = subBuckets.get(bucketHashKey);
				if (subBucket.size() > 1) {
					orderingSensitiveCloneGroupCandidates.put(bucketHashKey,
							subBucket);
					groupCloneFragmentsByScript(scriptToCloneCandidateSet,
							subBucket);
				}
			}
		}
		logger.log(Level.INFO, "Elimination overlapping fragments");
		eliminateOverlapFragmentsWithinScript(scriptToCloneCandidateSet,
				orderingSensitiveCloneGroupCandidates);
		logger.log(Level.INFO, "Elimination non clones");
		eliminateNonClone(orderingSensitiveCloneGroupCandidates);

		logger.log(Level.INFO, "Finalize clones");
		List<Set<Fragment>> finalCloneGroups = new ArrayList<>();
		finalCloneGroups.addAll(orderingSensitiveCloneGroupCandidates.values());
		return finalCloneGroups;
	}

	public static Map<Integer, Set<Fragment>> checkCandidateClones(
			Map<Integer, Set<Fragment>> candidates) {

		Map<Integer, Set<Fragment>> orderingSensitiveBuckets = new HashMap<>();
		Map<Script, Set<Fragment>> fragmentByScript = new HashMap<>();
		for (Integer groupID : candidates.keySet()) {
			Map<Integer, Set<Fragment>> buckets = hashIntoOrderingSensitiveBucket(candidates
					.get(groupID));
			for (Integer bucketHashKey : buckets.keySet()) {
				Set<Fragment> toIncludeBucket = buckets.get(bucketHashKey);
				if (toIncludeBucket.size() > 1) {
					orderingSensitiveBuckets
							.put(bucketHashKey, toIncludeBucket);
					groupCloneFragmentsByScript(fragmentByScript,
							toIncludeBucket);
				}
			}
		}
		eliminateOverlapFragmentsWithinScript(fragmentByScript,
				orderingSensitiveBuckets);
		eliminateNonClone(orderingSensitiveBuckets);

		return orderingSensitiveBuckets;
	}

	private static void eliminateNonClone(
			Map<Integer, Set<Fragment>> orderingSensitiveBuckets) {
		Iterator<Map.Entry<Integer, Set<Fragment>>> iter = orderingSensitiveBuckets
				.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<Integer, Set<Fragment>> entry = iter.next();
			if (entry.getValue().size() < 2) {
				iter.remove();
			}
		}
	}

	private static void eliminateOverlapFragmentsWithinScript(
			Map<Script, Set<Fragment>> fragmentByScript,
			Map<Integer, Set<Fragment>> orderingSensitiveBuckets) {
		int count = 0;
		for (Script script : fragmentByScript.keySet()) {
			List<Fragment> fragmentsWithinScript = new ArrayList<Fragment>(
					fragmentByScript.get(script));
			if (fragmentsWithinScript.size() == 1006) {
				System.out.println(fragmentsWithinScript.size());
			}

			count++;
			logger.log(Level.INFO, "eliminating candiates in sprite " + count
					+ "/" + fragmentByScript.size());
			logger.log(Level.INFO, "Sorting fragments...");
			Collections.sort(fragmentsWithinScript, fragmentComparator); // descending
																			// order
																			// by
																			// size

			while (!fragmentsWithinScript.isEmpty()) {
				Iterator<Fragment> fragmentIter = fragmentsWithinScript
						.iterator();
				logger.log(Level.INFO, fragmentsWithinScript.size()
						+ " fragments within script");
				int fragmentIdx = 0;

				List<Fragment> toBeRemoved = new ArrayList<>();
				if (fragmentIter.hasNext()) {
					fragmentIdx++;
					// logger.log(Level.INFO,"looking for all fragments overlapping with fragment "
					// + fragmentIdx + "/"+fragmentsWithinScript.size());
					Fragment current = fragmentIter.next();
					fragmentIter.remove();

					int otherIdx = 0;
					for (Fragment other : fragmentsWithinScript) {
						otherIdx++;
						// logger.log(Level.INFO, otherIdx+"");
						if (current != other) {
							boolean isDisjoint = computeOverlap(current, other);

							if (!isDisjoint) { // overlap
								orderingSensitiveBuckets.get(other.getHash())
										.remove(other);
								toBeRemoved.add(other); // this is added
							}
						}
					}
				}
				boolean isRemovedSuccessful = fragmentsWithinScript
						.removeAll(toBeRemoved);
				if (isRemovedSuccessful) {
					logger.log(Level.INFO, toBeRemoved.size()
							+ " fragments are removed");
				}
			}
		}
	}

	public static boolean computeOverlap(Fragment current, Fragment other) {
		int curEndPos = current.getEndPos();
		int otherStartPos = other.getStartPos();
		if (otherStartPos < curEndPos) {
			return false;
		} else {
			return true;
		}
	}

	public static Comparator<Fragment> fragmentComparator = new Comparator<Fragment>() {
		@Override
		public int compare(Fragment fragment1, Fragment fragment2) {
			return fragment1.compareTo(fragment2);
		}
	};
	private Map<Scriptable, List<Set<Fragment>>> scriptableToCloneGroups;

	public static void groupCloneFragmentsByScript(
			Map<Script, Set<Fragment>> fragmentByScript,
			Set<Fragment> bucketContainsCandidateCloneFragmentForGrouping) {
		for (Fragment fragment : bucketContainsCandidateCloneFragmentForGrouping) {
			Script parentScript = fragment.blocks.get(0).parentScript();
			if (!fragmentByScript.containsKey(parentScript)) {
				fragmentByScript.put(parentScript, new HashSet<Fragment>());
			}
			fragmentByScript.get(parentScript).add(fragment);
		}
	}

	public static Map<Integer, Set<Fragment>> hashIntoOrderingSensitiveBucket(
			Set<Fragment> candidateClones) {
		Map<Integer, Set<Fragment>> orderingSensitiveBuckets = new HashMap<>();
		for (Fragment fragment : candidateClones) {
			List<ScratchBlock> serializedFragment = fragment.serialize();
			int hashValue = Fragment.hashSequenceOfBlocks(serializedFragment);
			if (!orderingSensitiveBuckets.containsKey(hashValue)) {
				orderingSensitiveBuckets
						.put(hashValue, new HashSet<Fragment>());
			}
			orderingSensitiveBuckets.get(hashValue).add(fragment);
		}
		return orderingSensitiveBuckets;
	}

	public static List<Set<Fragment>> analyzeForClones(Scriptable scriptable) {
		List<Set<Fragment>> candidates = new ArrayList<>();
		candidates.addAll(DuplicatedCodeAnalysis
				.analyzeForSubtreeCloneCandidate(scriptable).values());
		candidates.addAll(DuplicatedCodeAnalysis
				.analyzeForForestCloneCandidate(scriptable).values());
		return candidates;
	}

	@Override
	protected void buildFullReport() {
		fullReport.put("name", "Duplicated Code");
		List<ReportItem> instances = new ArrayList<ReportItem>();

		for (Scriptable scriptable : scriptableToCloneGroups.keySet()) {
			List<Set<Fragment>> cloneGroupsWithinScriptable = scriptableToCloneGroups
					.get(scriptable);
			List<List<String>> cloneGroupReport = new ArrayList<>();

			for (Set<Fragment> group : cloneGroupsWithinScriptable) {
				List<String> clones = new ArrayList<>();
				for (Fragment f : group) {
					clones.add(f.render());
				}
				cloneGroupReport.add(clones);
			}
			instances
					.add(new ReportItem(scriptable.getName(), cloneGroupReport));
		}
		fullReport.put("scriptables", instances);
	}

	public class ReportItem {
		private String scriptable;
		private List<List<String>> instances;

		private ReportItem(String name, List<List<String>> cloneGroups) {
			this.scriptable = name;
			this.instances = cloneGroups;
		}
	}

	/*
	 * --structure-- { name: "DC", density: 5, clone_group_size: 2, clone_size:
	 * 15 } - density is number of instances per 100 blocks
	 */

	@Override
	protected void buildShortReport() {
		shortReport.put("name", "DC");
		int totalBlocks = program.getTotalNumberOfBlocks();
		double numKBlocks = (double) totalBlocks / 100;

		List<Number> cloneGroupSizes = new ArrayList<>();
		List<Number> cloneSizes = new ArrayList();

		int instanceCount = 0;

		for (Scriptable scriptable : scriptableToCloneGroups.keySet()) {
			List<Set<Fragment>> cloneGroupsWithinScriptable = scriptableToCloneGroups
					.get(scriptable);
			for (Set<Fragment> group : cloneGroupsWithinScriptable) {
				instanceCount++;
				cloneGroupSizes.add( group.size());
				Fragment sample = group.iterator().next();
				cloneSizes.add(sample.getSize());
			}
		}
		if(numKBlocks==0){
			shortReport.put("density", null);
			shortReport.put("count", null);
		}else{
			shortReport.put("density", (double) instanceCount / numKBlocks);
			shortReport.put("count", (double) instanceCount);
		}
		shortReport.put("clone_size_dist", Util.toWeighedValues(cloneSizes));
		shortReport.put("group_size_dist", Util.toWeighedValues(cloneGroupSizes));
	}

}
