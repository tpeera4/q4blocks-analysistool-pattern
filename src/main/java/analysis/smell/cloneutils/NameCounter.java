package analysis.smell.cloneutils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeSet;

import main.ScratchBlocksMapper;
import AST.ASTNode;
import AST.ScratchBlock;

public class NameCounter {
	double[] counter;
	public int nodeCount = 0;
	List<ScratchBlock> addedNode;

	public NameCounter() {
		counter = new double[ScratchBlocksMapper.id2name.size()];
		addedNode = new ArrayList<>();
	}

	public void put(ScratchBlock block) {
		try {
			int id = ScratchBlocksMapper.getIDFromName(block.blockName());
			counter[id] = counter[id] + 1;
			addedNode.add(block);
			nodeCount++;
		} catch (Exception e) {
			System.err
					.println("No node name: " + block.blockName() + " found!");
		}
	}

	public void putAll(NameCounter counter) {
		for (ScratchBlock block : counter.getAddedNode()) {
			put(block);
		}
	}

	private List<ScratchBlock> getAddedNode() {
		return addedNode;
	}

	public double[] getVector() {
		return counter;
	}

	public int getNodeCount() {
		return nodeCount;
	}

	@Override
	public String toString() {
		return "nodeCount=" + nodeCount + ", addedNode=" + addedNode + "\n"
				+ Arrays.toString(counter);
	}

}
