package analysis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import main.Util;
import AST.Scriptable;
import AST.Sprite;

public class ProgramInfoAnalysis extends Analysis {
	
	
	@Override
	public void initialize() {
		// TODO Auto-generated method stub

	}

	@Override
	public void performAnalysis() {

	}



	@Override
	protected void buildFullReport() {

	}

	@Override
	protected void buildShortReport() {
		shortReport.put("name", "COSTUMES");
		for(Scriptable s : program.getAllScriptables()) {
			shortReport.put(s.getName(), s.getCostume(0).getValue());
		}
	}

	

}
