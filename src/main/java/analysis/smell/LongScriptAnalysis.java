package analysis.smell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import main.Util;

import org.jsoup.select.Evaluator.IsNthChild;

import AST.Script;
import AST.Scriptable;
import analysis.Analysis;
import analysis.ReportItem;

public class LongScriptAnalysis extends Analysis {
	public static int threshold = 11;
	private Map<Scriptable, HashSet<Script>> scriptableToLongScripts;
	private List<Number> lengthDis;
	
	@Override
	public void initialize() {
		scriptableToLongScripts = new HashMap<>();
		lengthDis = new ArrayList<Number>();
	}

	@Override
	public void performAnalysis() {
		for (Scriptable scriptable : program.getAllScriptables()) {
			for (Script script : scriptable.getScriptList()) {
				if (script.getLineOfCode() >= threshold) {
					if (!scriptableToLongScripts.containsKey(scriptable)) {
						scriptableToLongScripts.put(scriptable,
								new HashSet<Script>());
					}
					scriptableToLongScripts.get(scriptable).add(script);
					lengthDis.add(script.getLineOfCode());
				}
			}
		}
	}

	@Override
	protected void buildFullReport() {
		fullReport.put("name", "Long Script");
		List<ReportItem> instances = new ArrayList<ReportItem>();
		for (Scriptable scriptable : program.getAllScriptables()) {
			HashSet<String> LongScriptStr = new HashSet<>();
			for (Script script : scriptable.getScriptList()) {
				if (script.getLineOfCode() > threshold) {
					LongScriptStr.add(script.render());
				}
			}
			instances.add(new ReportItem(scriptable.getName(), LongScriptStr));
		}
		fullReport.put("scriptables", instances);

	}

	@Override
	protected void buildShortReport() {
		shortReport.put("name", "LS");
		int instanceCount = 0;
		int totalNumberOfScript = program.getTotalNumberOfScript();
		for (Scriptable scriptable : program.getAllScriptables()) {
			for (Script script : scriptable.getScriptList()) {
				if (script.getLineOfCode() > threshold) {
					instanceCount++;
				}
			}
		}

		if (totalNumberOfScript == 0) {
			shortReport.put("percentage", null);
			shortReport.put("count", null);
		} else {
			shortReport.put("percentage", (double) instanceCount*100
					/ totalNumberOfScript);
			shortReport.put("count", (double) instanceCount);
			shortReport.put("length_dist", Util.toWeighedValues(lengthDis));
		}
	}

}
