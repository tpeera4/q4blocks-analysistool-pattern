package main;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;

import AST.Program;
import AST.Scriptable;
import analysis.Analysis;
import analysis.ProgramInfoAnalysis;
import analysis.smell.BadNamingAnalysis;
import analysis.smell.BroadVariableScopeAnalysis;
import analysis.smell.DeadCodeAnalysis;
import analysis.smell.DuplicatedCodeAnalysis;
import analysis.smell.DuplicatedStringAnalysis;
import analysis.smell.FeatureEnvyAnalysis;
import analysis.smell.InappropriateIntimacyAnalysis;
import analysis.smell.LongScriptAnalysis;
import analysis.smell.MiddleManAnalysis;
import analysis.smell.NoOpAnalysis;
import analysis.smell.UndefinedBlockAnalysis;
import analysis.smell.UnusedVariableAnalysis;

public class AnalysisManager {

	public static List<Class> analysisClasses;
	public static Gson gson = new GsonBuilder().serializeNulls().disableHtmlEscaping().create();
	private static JsonObject costumeInfo;

	public static String process(Program program) {
		JsonObject fullSmellReports = new JsonObject();
		JsonArray otherSmellReports = new JsonArray();
		Map<String, JsonObject> scriptableToReports = new HashMap<>();
		for (Scriptable scriptable : program.getAllScriptables()) {
			scriptableToReports.put(scriptable.getName(), new JsonObject());
		}

		configureAnalyses();

		for (Class<Analysis> analysisClass : analysisClasses) {
			try {
				Analysis analysis = (Analysis) analysisClass.newInstance();
				analysis.setInputProgram(program);
				analysis.performAnalysis();

				// smell by scriptable
				processFullReportForEachScriptable(scriptableToReports, analysis);

				// other smells
				processOtherCustomSmellReports(otherSmellReports, analysis);

			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// costume info
		ProgramInfoAnalysis programInfoAnalysis = new ProgramInfoAnalysis();
		programInfoAnalysis.setInputProgram(program);
		programInfoAnalysis.performAnalysis();
		costumeInfo = programInfoAnalysis.getShortReportAsJsonObject();

		finalizeFullReport(fullSmellReports, scriptableToReports, otherSmellReports);

		String json = gson.toJson(fullSmellReports);
		return json;
	}

	private static void finalizeFullReport(JsonObject fullSmellReports, Map<String, JsonObject> scriptableToReports,
			JsonArray otherSmellReports) {
		// per sprite
		JsonArray reportBySprite = new JsonArray();
		for (String scriptable : scriptableToReports.keySet()) {
			JsonObject reportsPerScriptable = new JsonObject();
			reportsPerScriptable.addProperty("scriptable", scriptable);
			reportsPerScriptable.add("smells", scriptableToReports.get(scriptable));
			// costume info
			reportsPerScriptable.addProperty("costume", costumeInfo.get(scriptable).getAsString());
			reportBySprite.add(reportsPerScriptable);
		}

		fullSmellReports.add("SmellsPerSprite", reportBySprite);

		// others
		fullSmellReports.add("Others", otherSmellReports);
	}

	public static void processFullReportForEachScriptable(Map<String, JsonObject> scriptableToReports,
			Analysis analysis) {
		JsonObject jsonObj = analysis.getFullReportAsJsonObject();
		// smell by sprite
		String smellName = jsonObj.get("name").getAsString();
		if (jsonObj.has("scriptables")) {
			JsonArray scriptables = jsonObj.get("scriptables").getAsJsonArray();
			for (JsonElement scriptable : scriptables) {
				JsonObject scriptableObj = scriptable.getAsJsonObject();
				String scriptableName = scriptableObj.get("scriptable").getAsString();
				JsonObject reportsForTheScriptable = scriptableToReports.get(scriptableName);
				JsonArray instances = scriptableObj.get("instances").getAsJsonArray();
				if (instances.size() != 0) {
					reportsForTheScriptable.add(smellName, instances);
				}
			}
		}

	}

	private static void processOtherCustomSmellReports(JsonArray otherSmellReports, Analysis analysis) {
		String[] otherSmellsConsidered = new String[] { InappropriateIntimacyAnalysis.SMELL_NAME,
				FeatureEnvyAnalysis.SMELL_NAME, DuplicatedStringAnalysis.SMELL_NAME, MiddleManAnalysis.SMELL_NAME };

		List<String> others = Arrays.asList(otherSmellsConsidered);

		JsonObject report = analysis.getFullReportAsJsonObject();
		String smellName = report.get("name").getAsString();
		if (others.contains(smellName)) {
			otherSmellReports.add(report);
		}

		// if(smellName.equals(InappropriateIntimacyAnalysis.SMELL_NAME)) {
		// System.out.println(smellName);
		// System.out.println(report);
		// }
		// if(smellName.equals(FeatureEnvyAnalysis.SMELL_NAME)) {
		// System.out.println(smellName);
		// System.out.println(report);
		// }
		// if(smellName.equals(DuplicatedStringAnalysis.SMELL_NAME)) {
		// System.out.println(smellName);
		// System.out.println(report);
		// }
		// if(smellName.equals(MiddleManAnalysis.SMELL_NAME)) {
		// System.out.println(smellName);
		// System.out.println(report);
		// }

	}

	public static String processForShortReport(Program program) {
		JsonObject shortReports = new JsonObject();
		shortReports.addProperty("_id", program.getProjectID());

		configureAnalyses();
		for (Class<Analysis> analysisClass : analysisClasses) {
			try {
				Analysis analysis = (Analysis) analysisClass.newInstance();
				analysis.setInputProgram(program);
				analysis.performAnalysis();

				processShortReport(analysis, shortReports);
			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		String json = gson.toJson(shortReports);

		return json;
	}

	private static void processShortReport(Analysis analysis, JsonObject aggregateShortReport) {
		JsonObject shortReportObj = analysis.getShortReportAsJsonObject();
		String smellName = shortReportObj.get("name").getAsString();
		for (Entry<String, JsonElement> entry : shortReportObj.entrySet()) {
			if (!entry.getKey().equals("name")) {
				if (!entry.getValue().isJsonNull()) {
					aggregateShortReport.add(smellName + "_" + entry.getKey(), entry.getValue());
				} else {
					aggregateShortReport.add(smellName + "_" + entry.getKey(), JsonNull.INSTANCE);
				}
			}
		}
	}

	public static void configureAnalyses() {
		analysisClasses = new ArrayList<>();
		// analysisClasses.add(ProgramStatisticAnalysis.class);
		analysisClasses.add(BroadVariableScopeAnalysis.class);
		analysisClasses.add(DeadCodeAnalysis.class);
		analysisClasses.add(DuplicatedCodeAnalysis.class);
		analysisClasses.add(DuplicatedStringAnalysis.class);
		analysisClasses.add(FeatureEnvyAnalysis.class);
		analysisClasses.add(InappropriateIntimacyAnalysis.class);
		analysisClasses.add(LongScriptAnalysis.class);
		analysisClasses.add(MiddleManAnalysis.class);
		analysisClasses.add(NoOpAnalysis.class);
		analysisClasses.add(BadNamingAnalysis.class);
		analysisClasses.add(UndefinedBlockAnalysis.class);
		analysisClasses.add(UnusedVariableAnalysis.class);
	}

	public static String processWithTimer(Program program, boolean fullReport, int seconds)
			throws TimeoutException, ExecutionException {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<String> future = executor.submit(new AnalysisManager().new Task(program, fullReport));
		String result = null;
		try {
			// System.out.println("Started analyzing...");
			future.get(seconds, TimeUnit.SECONDS);
			result = future.get();
			// System.out.println("Finished!");
		} catch (TimeoutException e) {
			future.cancel(true);
			System.out.println("Terminated!");
			throw e;
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
			throw e;
		}

		executor.shutdownNow();

		return result;
	}

	class Task implements Callable<String> {
		private final Program program;
		private final boolean isFullReport;

		public Task(Program program, boolean fullReport) {
			this.program = program;
			this.isFullReport = fullReport;
		}

		@Override
		public String call() throws Exception {
			String jsonResult = null;
			if (isFullReport) {
				jsonResult = AnalysisManager.process(program);
			} else {
				jsonResult = AnalysisManager.processForShortReport(program);
			}
			return jsonResult;
		}
	}

	public static void main(String[] args) throws TimeoutException, ExecutionException {
		String FILENAME = "/test-smelly.json";
		Program program = null;
		StringReader srd = null;
		ScratchParser parser = new ScratchParser();
		Integer[] projectIDs = new Integer[] { 149974792, 10247904, 18296306, 28478704, 35669286, 42533394, 104751552,
				115688906, 123698059, 131757169, 143534482 };

		for (Integer projectID : projectIDs) {

			try {
				// srd = Util.getStringReaderFromFile(FILENAME);
				srd = Util.retrieveProjectOnline(projectID);
				program = parser.parse(srd);
			} catch (Throwable e) {
				e.printStackTrace();
			}

			// String json = AnalysisManager.processWithTimer(program, false, 60);
			String json = AnalysisManager.processForShortReport(program);
			// String json = AnalysisManager.process(program);
			System.out.println(json);
		}

	}

}