package analysis;

import java.io.StringReader;

import junit.framework.TestCase;
import main.ScratchParser;
import main.Util;
import AST.ASTNode;
import AST.CommandBlock;
import AST.Program;
import AST.Script;
import AST.Sprite;

public class CFGTest extends TestCase {
	private final static String FILENAME = "/test-input.txt";
	private final static int projectID = 151770183;
	Program program = null;
	
	@Override
	protected void setUp() {
		StringReader srd = null;
		  ScratchParser parser = new ScratchParser();
	      program = null;
	      
	      try {
	    	  srd = Util.retrieveProjectOnline(projectID);
//	          srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}

	public void test() {
		Sprite sprite = program.getStage().getSprite(0);
		Script script = sprite.getScript(0); 
		CommandBlock firstBlock = (CommandBlock) script.getBody().getBlocks(0);
//		System.out.println(script.CFGDotSpec());
		// ASTNode node = script.lookupNodeName("List_1", script);
		// System.out.print(node);
		
	}
	
	public void testVis(){
		
	}
}