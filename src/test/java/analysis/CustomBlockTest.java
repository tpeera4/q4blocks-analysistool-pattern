package analysis;

import java.io.StringReader;

import junit.framework.TestCase;
import main.ScratchParser;
import main.Util;
import AST.Program;

public class CustomBlockTest extends TestCase {
	private final static String FILENAME = "/test-input.txt";
	private final static int projectID = 154605937;
	private Program program;
	@Override
	protected void setUp() {
		StringReader srd = null;
		  ScratchParser parser = new ScratchParser();
	      program = null;
	      
	      try {
	    	  srd = Util.retrieveProjectOnline(projectID);
//	          srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}
	public void testGetParameterType(){
		System.out.println(program.render());
	}
}