package analysis.smell;

import java.io.StringReader;

import com.google.gson.JsonObject;

import junit.framework.TestCase;
import main.ScratchParser;
import main.Util;
import AST.Program;
import analysis.smell.DeadCodeAnalysis;

public class DeadCodeAnalysisTest extends TestCase {
	private final static String FILENAME = "/smell/test-Deadcode.json";
	Program program = null;
	
	@Override
	protected void setUp() {
		StringReader srd = null;
		  ScratchParser parser = new ScratchParser();
	      program = null;
	      
	      try {
	          srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}

	public void test() {
		DeadCodeAnalysis analysis = new DeadCodeAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		assertNotNull(analysis.getFullReportAsJsonObject());
		System.out.println(analysis.getFullReportAsJsonObject());
		JsonObject shortReportObj = analysis.getShortReportAsJsonObject();
		assertEquals(14.285, shortReportObj.get("density").getAsDouble(), 0.1);
	}
}