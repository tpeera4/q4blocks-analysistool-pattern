package analysis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import main.Util;
import AST.Sprite;

public class ProgramStatisticAnalysis extends Analysis {
	
	
	@Override
	public void initialize() {
		// TODO Auto-generated method stub

	}

	@Override
	public void performAnalysis() {

	}



	@Override
	protected void buildFullReport() {

	}

	@Override
	protected void buildShortReport() {
		shortReport.put("name", "METRIC");
		shortReport.put("numVar", program.getTotalNumberOfVariables());
		shortReport.put("numSprite", program.getAllScriptables().size());
		shortReport.put("numProc", program.getTotalNumberOfProcedure());
		shortReport.put("numScript", program.getTotalNumberOfScript());
		shortReport.put("numBlock", program.getTotalNumberOfBlocks());
		shortReport.put("numComment", program.allComments().size());
		shortReport.put("script_length_dist", Util.toWeighedValues(program.getLineOfCodeValueColl()));
	}

	

}
