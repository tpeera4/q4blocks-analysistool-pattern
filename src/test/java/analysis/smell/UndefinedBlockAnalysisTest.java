package analysis.smell;

import java.io.StringReader;

import com.google.gson.JsonObject;

import junit.framework.TestCase;
import main.ScratchParser;
import main.Util;
import AST.Program;
import analysis.smell.UndefinedBlockAnalysis;

public class UndefinedBlockAnalysisTest extends TestCase {
	private final static String FILENAME = "/smell/test-UndefinedBlock.json";
	private final static int projectID = 151770183;
	Program program = null;
	
	@Override
	protected void setUp() {
		StringReader srd = null;
		  ScratchParser parser = new ScratchParser();
	      program = null;
	      
	      try {
//	    	  srd = Util.retrieveProjectOnline(projectID);
	          srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}

	public void test() {
		UndefinedBlockAnalysis analysis = new UndefinedBlockAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		String result = analysis.getFullReportAsJson();
		System.out.println(result);
		JsonObject shortJsonReportObj = analysis.getShortReportAsJsonObject();
		assertTrue(shortJsonReportObj.get("count").getAsInt()>0);
		
		String shortReport = analysis.getShortReportAsJson();
		System.out.println(shortReport);
		
	}
}