package analysis.smell;

import java.io.StringReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import junit.framework.TestCase;
import main.ScratchParser;
import main.Util;
import AST.Program;
import analysis.smell.InappropriateIntimacyAnalysis;

public class InappropriateIntimacyAnalysisTest extends TestCase {
	private final static String FILENAME = "/smell/test-InappropriateIntimacy.json";
	private final static int projectID = 151770183;
	Program program = null;

	@Override
	protected void setUp() {
		StringReader srd = null;
		ScratchParser parser = new ScratchParser();
		program = null;
		InappropriateIntimacyAnalysis.threshold = 2;

		try {
			// srd = Util.retrieveProjectOnline(projectID);
			srd = Util.getStringReaderFromFile(FILENAME);
			program = parser.parse(srd);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void test() {
		InappropriateIntimacyAnalysis analysis = new InappropriateIntimacyAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		String fullReport = analysis.getFullReportAsJson();
		System.out.println(fullReport);

		JsonObject shortReportJsonObj = analysis.getShortReportAsJsonObject();
		assertEquals(1, shortReportJsonObj.get("count").getAsInt());
		System.out.println(analysis.getShortReportAsJson());

//		JsonArray dist = new JsonArray();
//		dist.add(2);
//		dist.add(1);
//		assertEquals(dist, shortReportJsonObj.get("dist"));

	}
}