package analysis.smell;

import java.io.StringReader;

import com.google.gson.JsonObject;

import junit.framework.TestCase;
import main.ScratchParser;
import main.Util;
import AST.Program;
import analysis.smell.BadNamingAnalysis;

public class BadNamingAnalysisTest extends TestCase {
	private final static String FILENAME = "/smell/test-BadName.json";
	private final static int projectID = 152433266;
	Program program = null;
	
	@Override
	protected void setUp() {
		StringReader srd = null;
		  ScratchParser parser = new ScratchParser();
	      program = null;
	      
	      try {
//	    	  srd = Util.retrieveProjectOnline(projectID);
	          srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}

	public void test() {
		BadNamingAnalysis analysis = new BadNamingAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		String fullJsonReport = analysis.getFullReportAsJson();
		System.out.println(fullJsonReport);
		JsonObject shortReport = analysis.getShortReportAsJsonObject();
		System.out.println(analysis.getShortReportAsJson());
		int count = shortReport.get("count").getAsInt();
		int numSprite = program.getStage().getNumSprite();
		assertEquals(count*100/numSprite, shortReport.get("percentage").getAsDouble(),0.01);
	}
}