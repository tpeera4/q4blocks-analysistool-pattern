package main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.rank.Median;
import org.jsoup.Jsoup;

import com.google.common.primitives.Doubles;

public class Util {
	
	public static Mean mean = new Mean();
	public static Median median = new Median();
	
	public static double evaluateMedian(List<Number> values){
		return median.evaluate(Doubles.toArray(values));
	}
	
	public static StringReader getStringReaderFromFile(String path) throws FileNotFoundException {
		StringReader srd = null;
		BufferedReader rd = null;

		try {
			URL url = Main.class.getResource(path);
			InputStreamReader dataReader = new InputStreamReader(
					url.openStream());
			rd = new BufferedReader(dataReader);

			String inputLine = null;
			StringBuilder builder = new StringBuilder();

			while ((inputLine = rd.readLine()) != null)
				builder.append(inputLine);

			srd = new StringReader(builder.toString());
		} catch (NullPointerException e) {
			throw new FileNotFoundException();
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return srd;

	}

	public static String unquote(String quoted) {
		return quoted.substring(1, quoted.length() - 1);
	}
	
	
	public static final String baseDownLoadURL = "http://projects.scratch.mit.edu/internalapi/project/%1$d/get/";
	
	public static StringReader retrieveProjectOnline(int projectID) throws IOException{
		String doc = retrieveProjectOnlineAsString(projectID);
		StringReader srd = new StringReader(doc);
		return srd;
	}
	
	public static String retrieveProjectOnlineAsString(int projectID) throws IOException{
		
		String URL = String.format(baseDownLoadURL,projectID);
		String doc = Jsoup.connect(URL).ignoreContentType(true).execute().body();
		
		return doc;
	}
	
	public static Map<Object, Number> toWeighedValues(List<Number> listOfNumber) {
		Map<Object, Number> weightedVals = new HashMap<Object, Number>();
		for(Number val : listOfNumber){
			if(!weightedVals.containsKey(val)){
				weightedVals.put(val, 0);
			}
			int count = weightedVals.get(val).intValue()+1;
			weightedVals.put(val, count);
		}
		return weightedVals;
	}
	
}