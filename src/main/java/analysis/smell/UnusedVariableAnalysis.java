package analysis.smell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import AST.ScratchBlock;
import AST.Scriptable;
import AST.VarDecl;
import analysis.Analysis;
import analysis.ReportItem;

public class UnusedVariableAnalysis extends Analysis {
	HashMap<Scriptable, HashSet<VarDecl>> scriptableToUnusedVars;

	@Override
	public void initialize() {
		scriptableToUnusedVars = new HashMap<>();
	}

	@Override
	public void performAnalysis() {
		for (Scriptable scriptable : program.getAllScriptables()) {
			for (VarDecl var : scriptable.getVarDeclList()) {
				Set<ScratchBlock> usages = program.allVariableUsagesOf(var);
				if (!usages.isEmpty()) {
					continue;
				}
				if (!scriptableToUnusedVars.containsKey(scriptable)) {
					scriptableToUnusedVars.put(scriptable,
							new HashSet<VarDecl>());
				}
				scriptableToUnusedVars.get(scriptable).add(var);
			}
		}
	}

	@Override
	protected void buildFullReport() {
		fullReport.put("name", "Unused Variable");
		List<ReportItem> instances = new ArrayList<ReportItem>();

		for (Scriptable scriptable : scriptableToUnusedVars.keySet()) {
			HashSet<String> unusedVarStr = new HashSet<>();
			for (VarDecl var : scriptableToUnusedVars.get(scriptable)) {
				unusedVarStr.add(var.render());
			}
			instances.add(new ReportItem(scriptable.getName(), unusedVarStr));
		}
		fullReport.put("scriptables", instances);

	}

	@Override
	protected void buildShortReport() {
		shortReport.put("name", "UV");
		int instanceCount = 0;
		int allVariables = program.variableDeclarations().size();
		for (Scriptable scriptable : scriptableToUnusedVars.keySet()) {
			instanceCount += scriptableToUnusedVars.get(scriptable).size();
		}
		if(allVariables==0){
			shortReport.put("percentage", null);
		}else{
			shortReport.put("percentage", (double) instanceCount*100 / allVariables);
		}
	}
}
