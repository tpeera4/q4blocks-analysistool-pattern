package analysis.smell;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import junit.framework.TestCase;
import main.ScratchParser;
import main.Util;
import AST.CommandBlock;
import AST.Expression;
import AST.Program;
import AST.StringLiteral;
import analysis.smell.DuplicatedStringAnalysis;

public class DuplicatedStringAnalysisTest extends TestCase {
	private final static String FILENAME = "/smell/test-DuplicatedString.json";
	private final static int projectID = 149974792;
	Program program = null;
	StringReader srd = null;
	ScratchParser parser = new ScratchParser();
	
	List<String> ignoreBlocks = Arrays.asList(new String[]{""});
	
	@Override
	protected void setUp() {
	      program = null;
	      
	      try {
//	    	  srd = Util.retrieveProjectOnline(projectID);
	          srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}
	
	public void test() {
		DuplicatedStringAnalysis analysis = new DuplicatedStringAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		String fullReport = analysis.getFullReportAsJson();
		String shortReport = analysis.getShortReportAsJson();
		System.out.println(fullReport);
		System.out.println(shortReport);
		JsonObject jsonShortReport = analysis.getShortReportAsJsonObject();
		assertEquals(33.33, jsonShortReport.get("percentage").getAsDouble(),0.01);
	}
	
}