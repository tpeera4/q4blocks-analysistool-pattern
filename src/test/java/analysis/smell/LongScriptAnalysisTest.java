package analysis.smell;

import java.io.StringReader;

import junit.framework.TestCase;
import main.ScratchParser;
import main.Util;
import AST.Program;
import analysis.smell.LongScriptAnalysis;

public class LongScriptAnalysisTest extends TestCase {
	private final static String FILENAME = "/smell/test-LongScript.json";
	private final static int projectID = 152433266;
	Program program = null;
	
	@Override
	protected void setUp() {
		StringReader srd = null;
		  ScratchParser parser = new ScratchParser();
	      program = null;
	      LongScriptAnalysis.threshold = 11;
	      
	      try {
//	    	  srd = Util.retrieveProjectOnline(projectID);
	          srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}

	public void test() {
		int loc = program.getStage().getSprite(0).getScript(0).getLineOfCode();
		assertEquals(12, loc);
		System.out.println(loc);
		LongScriptAnalysis analysis = new LongScriptAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		String fullJsonReport = analysis.getFullReportAsJson();
		System.out.println(fullJsonReport);
		
		System.out.println(analysis.getShortReportAsJson());
	}
}