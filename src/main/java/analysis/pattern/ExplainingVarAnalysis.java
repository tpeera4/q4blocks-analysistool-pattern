package analysis.pattern;

import analysis.Analysis;

public class ExplainingVarAnalysis extends Analysis{
	int count = 0;
	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void performAnalysis() {
		count = program.getTotalNumberOfVariables();
	}

	@Override
	protected void buildFullReport() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void buildShortReport() {
		shortReport.put("name", "EV");
		shortReport.put("count", count);
		
	}

}
