package main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.List;

import AST.Comment;
import AST.Costume;
import AST.ParseException;
import AST.Program;
import AST.Scratch;
import AST.Script;
import AST.Scriptable;
import AST.Sprite;
import AST.Stage;
import AST.VarDecl;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ScratchParser {
	JsonParser jsonParser = new JsonParser();

	public ScratchParser() {
		// TODO Auto-generated constructor stub
	}

	public Program parse(StringReader srd) throws Exception {
		Program program = new Program();
		Stage stage = new Stage();
		stage.setName("_stage_");
		JsonElement jelement = jsonParser.parse(srd);
		JsonObject stageObj = jelement.getAsJsonObject();
		try{
			program.setProjectID(stageObj.get("info").getAsJsonObject().get("projectID").getAsInt());
		}catch(Exception e){
			
		}
		parseVar(stageObj, stage);
		parseScriptable(stageObj, stage);
		

		JsonArray children = stageObj.get("children").getAsJsonArray();
		for (int childIdx = 0; childIdx < children.size(); childIdx++) {
			JsonObject child = children.get(childIdx).getAsJsonObject();
			parseSprite(child, stage);
		}
		program.setStage(stage);
		return program;
	}

	public void parseSprite(JsonObject child, Stage stage) throws Exception {
		if (child.has("objName")) {
			Sprite sprite = new Sprite();
			sprite.setName(child.get("objName").getAsString());
			parseVar(child, sprite);
			parseScriptable(child, sprite);
			stage.addSprite(sprite);
		}
	}

	public void parseScriptable(JsonObject child, Scriptable sprite) throws ParseException {
		//non code
		augmentScriptableWithCostumeInfo(child.get("costumes"), sprite);
		//code
		if (child.has("scripts")) {
			JsonArray scripts = child.get("scripts").getAsJsonArray();
			if(child.has("scriptComments")){
				augmentScriptWithComment(child.get("scriptComments"), sprite);
			}
			
			for (int scriptIdx = 0; scriptIdx < scripts.size(); scriptIdx++) {
				JsonElement scriptJsonObj = scripts.get(
						scriptIdx);
				String scriptString = scriptJsonObj.toString();
				Script script = parseScript(scriptString);
				
//					System.err.println(scripts.get(scriptIdx).toString());
//					e.printStackTrace();
//					throw new Exception(scripts.get(scriptIdx).toString());
//				}
				sprite.addScript(script);
			}
		}
	}

	private void augmentScriptableWithCostumeInfo(JsonElement jsonElement, Scriptable scriptable) {
		JsonArray jsonArrCostumes = jsonElement.getAsJsonArray();
		for(JsonElement costumeJsonEl : jsonArrCostumes){
			Costume costume = new Costume();
			costume.setValue(costumeJsonEl.getAsJsonObject().get("baseLayerMD5").getAsString());
			scriptable.addCostume(costume);
		}
	}

	private void augmentScriptWithComment(JsonElement jsonElement, Scriptable scriptable) {
		JsonArray jsonArrComments = jsonElement.getAsJsonArray();
		for(JsonElement commentJsonEl : jsonArrComments){
			Comment comment = new Comment();
			comment.setValue(commentJsonEl.getAsJsonArray().get(6).getAsString());
			scriptable.addComment(comment);
		}
		
	}
	

	public static Script parseScript(String scriptString) throws ParseException {
		StringReader scriptSRD = new StringReader(scriptString);
		Script script = null;
			script = new Scratch(scriptSRD).Script();
		return script;
	}

	public void parseVar(JsonObject scriptableObj, Scriptable scriptable) {
		if (scriptableObj.has("variables")) {
			JsonArray globalVars = scriptableObj.get("variables")
					.getAsJsonArray();
			for (int varIdx = 0; varIdx < globalVars.size(); varIdx++) {
				JsonObject varObj = globalVars.get(varIdx).getAsJsonObject();
				VarDecl varDeclNode = new VarDecl();
				varDeclNode.setName(varObj.get("name").getAsString());
				varDeclNode.setValue(varObj.get("value").getAsString());
				scriptable.addVarDecl(varDeclNode);
			}
		}
	}
	
	
	private final static String FILENAME = "/test-BroadVariableScope.json"; //path in the jar

    public static void main(String[] args) {
      StringReader srd = null;
      BufferedReader rd = null;
      Program program = null;
      Stage stage = null;
      
      try {
          URL url = Main.class.getResource(FILENAME);
          InputStreamReader dataReader = new InputStreamReader(url.openStream());
          rd = new BufferedReader(dataReader);
      
          String inputLine = null;
          StringBuilder builder = new StringBuilder();

          while((inputLine = rd.readLine()) != null)
            builder.append(inputLine);
          
//          srd = new StringReader(builder.toString());
//          srd = Util.retrieveProjectOnline(17828009);
          srd = Util.retrieveProjectOnline(152030022);
          ScratchParser parser = new ScratchParser();
          program = parser.parse(srd);
          System.out.println("Syntax is okay");

      } catch (Throwable e) {
          // Catching Throwable is ugly but JavaCC throws Error objects!
          System.out.println("Syntax check failed: " + e.getMessage());
          e.printStackTrace();
      }
//      program.computeBroadVariableScope();
       String result = program.render();
       System.out.println(result);
//       String json = AnalysisManager.process(program);
//       System.out.println(json);
    }
}
