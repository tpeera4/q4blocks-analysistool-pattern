package analysis.smell;

import java.io.StringReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import junit.framework.TestCase;
import main.ScratchParser;
import main.Util;
import AST.Program;
import analysis.smell.MiddleManAnalysis;

public class MiddleManAnalysisTest extends TestCase {
	private final static String FILENAME = "/smell/test-MiddleMan.json";
	private final static int projectID = 149974792;
	Program program = null;
	
	@Override
	protected void setUp() {
		StringReader srd = null;
		  ScratchParser parser = new ScratchParser();
	      program = null;
	      
	      try {
//	    	  srd = Util.retrieveProjectOnline(projectID);
	          srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}

	public void test() {
		MiddleManAnalysis analysis = new MiddleManAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		assertEquals(3,analysis.middleManPath.size());
		String fullReport = analysis.getFullReportAsJson();
		System.out.println(fullReport);
		JsonObject shortReportJsonObj = analysis.getShortReportAsJsonObject();
		assertEquals(3, shortReportJsonObj.get("count").getAsInt());
		String shortReport = analysis.getShortReportAsJson();
		System.out.println(shortReport);
		
		JsonArray chainLengthDis = new JsonArray();
		chainLengthDis.add(2);
		chainLengthDis.add(2);
//		assertEquals(chainLengthDis,shortReportJsonObj.get("chain_length_dist"));
	}
}