package analysis.smell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import AST.ASTNode;
import AST.BlockSeq;
import AST.CommandBlock;
import AST.Script;
import AST.Scriptable;
import analysis.Analysis;

public class NoOpAnalysis extends Analysis{
	Map<Scriptable, List<Script>> scriptableToNoOpScripts = new HashMap<>();
	int instanceCount = 0;
	@Override
	public void initialize() {
	}

	@Override
	public void performAnalysis() {
		for(CommandBlock block: program.allEventHatBlocksColl()){
			Script parentScript = block.parentScript();
			int numBlocks = parentScript.getBody().getNumBlocks();
			if(numBlocks == 1){
				if(!scriptableToNoOpScripts.containsKey(parentScript.spriteParent())) {					
					scriptableToNoOpScripts.put(parentScript.spriteParent(), new ArrayList<Script>());
				}
				scriptableToNoOpScripts.get(parentScript.spriteParent()).add(parentScript);
				instanceCount++;
			}
		}
	}

	@Override
	protected void buildFullReport() {
		fullReport.put("name", "No-Op");
		List<ReportItem> instances = new ArrayList<ReportItem>();
		for(Scriptable key: scriptableToNoOpScripts.keySet()){
			HashSet<String> noOpScriptAsString = new HashSet<String>();
			for(Script script: scriptableToNoOpScripts.get(key)){
				noOpScriptAsString.add(script.render());
			}
			instances.add(new ReportItem(key.getName(), noOpScriptAsString));
		}
		fullReport.put("scriptables", instances);
	}
	public static class ReportItem {
		private String scriptable;
		private HashSet<String> instances;

		ReportItem(String name, HashSet<String> scripts) {
			this.scriptable = name;
			this.instances = scripts;
		}
	}

	@Override
	protected void buildShortReport() {
		shortReport.put("name", "NO");
		shortReport.put("count", instanceCount);
	}

}
