# Todo

## Enhancement
[] finishing all code smell reports
[] Two type of ReportItem (Smells in each sprite, others(custom))

## Maintenance
[] add ParsingException -- main.ScratchParser.parse()
[] remove exception handling for setup() in all smell test suite
[] main in ScratchParser needs update (remove unnecessary code)
[] Refactor Report aspect as interface (separate it out of Analysis) 
[] allow analysis but not include the result in the report
# Done
[x] fix bug Duplicate String
[x] collect baseLayerMD5 to each scriptable

# Pending
test No-OP, MiddleMan	https://scratch.mit.edu/projects/162299571/
remove putIfAbsent (to make it work with JRE 7 of the GAE)

BroadcastAndWaitBlock is not included in the analysis

* model sensor block (local var access of other vars)


[] Make Graph toString() a separate specialize method


# Documentation
## Parsing

## Smell Analysis

## GraphGeneration


# Resources
- Get Json representation of the Scratch project:
http://projects.scratch.mit.edu/internalapi/project/PROJECT_ID/get/

- Online graph visualization tool using graphviz grammar
http://www.webgraphviz.com/


-----------------------------
# Pattern Analysis
## What analyses?
https://paper.dropbox.com/doc/Quality-Improving-Practices-for-Scratch-LCBJCEo54Y5o6irEKGr2y

## To make it consistent:
* Class name should be <PatternName>Analysis.java
* Unit testing class name should be <PatternName>AnalysisTest.java
* Save Scratch's project .sb2 used for testing in src/test-projects/ and naming it as 
test-pattern-<PatternName>.sb2
* Save Scratch's project .json used for testing in src/test/resources/pattern/ and naming it as test-<PatternName>.sb2

## Getting the Json representation
* http://projects.scratch.mit.edu/internalapi/project/{PROJECT_ID}/get/


