package analysis.smell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import AST.ScratchBlock;
import AST.Script;
import AST.Scriptable;
import AST.VarDecl;
import analysis.Analysis;

public class FeatureEnvyAnalysis extends Analysis {
	public static final String SMELL_NAME = "Feature Envy";
	HashMap<Scriptable, Set<Script>> scriptableToEnvySmell;
	List<ReportItem> instances;
	int count = 0;

	@Override
	public void initialize() {
		scriptableToEnvySmell = new HashMap<>();
		instances = new ArrayList<ReportItem>();
	}

	Set<InstanceItem> instancesSet = new HashSet<>();
	
	@Override
	public void performAnalysis() {
		/*
		 * look at each global variable to see if it's used to pass a result value from
		 * one scriptable to another keep track of consumer scriptables and the feature
		 * in another sprite that
		 */
		for (VarDecl var : program.getStage().getVarDeclList()) {
			Set<Scriptable> spriteConsumers = extractSpriteParent(var.getReads());
			Set<Scriptable> spriteProducers = extractSpriteParent(var.getWrites());

			if (spriteConsumers.size() == 1 && spriteProducers.size() == 1
					&& !spriteConsumers.containsAll(spriteProducers)) {
				Scriptable theOnlyConsumer = spriteConsumers.iterator().next();
				Scriptable theOnlyProducer = spriteProducers.iterator().next();
				InstanceItem instanceItem = new InstanceItem(theOnlyConsumer, theOnlyProducer, var, extractScripts(var.getWrites()));
				instancesSet.add(instanceItem);
			}
			count = instancesSet.size();

		}
	}

	private Set<Script> extractScripts(Set<ScratchBlock> writes) {
		Set<Script> featureScripts = new HashSet<>();
		for (ScratchBlock varWrite : writes) {
			featureScripts.add(varWrite.parentScript());
		}
		return featureScripts;
	}

	private Set<Scriptable> extractSpriteParent(Set<ScratchBlock> reads) {
		Set<Scriptable> parents = new HashSet<>();
		for (ScratchBlock varRead : reads) {
			parents.add(varRead.spriteParent());
		}
		return parents;
	}
	
	@Override
	protected void buildFullReport() {
		fullReport.put("name", SMELL_NAME);
		fullReport.put("instances", instancesSet);
	}

	public static class ReportItem {
		String scriptable;
		Set<InstanceItem> instances;

		ReportItem(String name, Set<InstanceItem> set) {
			this.scriptable = name;
			this.instances = set;
		}
	}

	public static class InstanceItem {
		@Override
		public String toString() {
			return "InstanceItem [enviousOf=" + enviousOf + ", variable=" + variable + ", scripts=" + locations + "]";
		}
		
		String scriptable;
		String enviousOf;
		String variable;
		HashSet<String> locations;

		InstanceItem(Scriptable theOnlyConsumer, Scriptable writer, VarDecl var, Set<Script> scripts) {
			this.scriptable = theOnlyConsumer.getName();
			this.enviousOf = writer.getName();
			this.variable = var.getName();
			HashSet<String> featureScriptStrSet = new HashSet<>();
			for (Script featureScript : scripts) {
				featureScriptStrSet.add(featureScript.render());
			}
			this.locations = featureScriptStrSet;
		}
	}

	@Override
	protected void buildShortReport() {
		int totalBlocks = program.getTotalNumberOfBlocks();

		shortReport.put("name", "FE");
		shortReport.put("count", count);

	}

}
