package analysis.smell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import AST.ScratchBlock;
import AST.Scriptable;
import AST.VarDecl;
import AST.VariableExpBlock;
import analysis.Analysis;
import analysis.ReportItem;

public class BroadVariableScopeAnalysis extends Analysis {
	HashMap<Scriptable, HashSet<VarDecl>> scriptableToBroadVars;

	@Override
	public void initialize() {
		scriptableToBroadVars = new HashMap<>();
	}

	@Override
	public void performAnalysis() {
		for (int vi = 0; vi < program.getStage().getNumVarDecl(); vi++) {
			VarDecl var = program.getStage().getVarDecl(vi);
			Set<ScratchBlock> usages = program.allVariableUsagesOf(var);
			Set<Scriptable> users = new HashSet();
			ScratchBlock[] usageList = usages.toArray(new ScratchBlock[usages
					.size()]);
			for (int ui = 0; ui < usageList.length; ui++) {
				Scriptable scriptable = usageList[ui].spriteParent();
				users.add(scriptable);
			}
			if (users.size() == 1) {
				if (users.contains(program.getStage())) {
					continue;
				}
				if (!scriptableToBroadVars.containsKey(users.iterator().next())) {
					scriptableToBroadVars.put(users.iterator().next(),
							new HashSet<VarDecl>());
				}
				scriptableToBroadVars.get(users.iterator().next()).add(var);
			}
		}
	}

	@Override
	protected void buildFullReport() {
		fullReport.put("name", "Broad Variable Scope");
		List<ReportItem> instances = new ArrayList<ReportItem>();

		for (Scriptable scriptable : scriptableToBroadVars.keySet()) {
			HashSet<String> broadVarStr = new HashSet<>();
			for (VarDecl var : scriptableToBroadVars.get(scriptable)) {
				broadVarStr.add(var.render());
			}
			instances.add(new ReportItem(scriptable.getName(), broadVarStr));
		}
		fullReport.put("scriptables", instances);
	}

	@Override
	protected void buildShortReport() {
		shortReport.put("name", "BV");
		int instanceCount = 0;
		int allVariables = program.variableDeclarations().size();
		for (Scriptable scriptable : scriptableToBroadVars.keySet()) {
			instanceCount += scriptableToBroadVars.get(scriptable).size();
		}
		if(allVariables==0){
			shortReport.put("percentage", null);
		}else{
			shortReport.put("percentage", (double) instanceCount*100 / allVariables);
		}
	}

}
